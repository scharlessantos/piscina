#include <EEPROM.h>
#include <DS3231.h>

struct Data {
  long pstart;
  long pduration;
  long pfinish;
  

  long lstart;
  long lduration;
  long lfinish;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         

} data;

DS3231 rtc(SDA, SCL);

int LED_PIN = 9;
int LED_LED = 10;
int LED_RELAY = 6;
int PUMP_PIN = 11; 
int PUMP_LED = 12;
int PUMP_RELAY = 5;
int BUZZER = 13;
int FILTER_PIN = 4;
int FILTER_LED = 3;

int ltime = 0;
int lprevious = LOW;
int lstatus = LOW;

int ptime = 0;
int pprevious = LOW;
int pstatus = LOW;
int fprevious = LOW;

int lleitura;
int pleitura;
int fleitura;

int lnleitura;
int pnleitura;

long tolerancia = 350;
long time = 0;

void write(int address, long value);

long read(long address);

int canChangeLed();

int canChangePump();

void checkStartPump();

void checkStopPump();

void checkStopLed();

void startPump();

void stopPump();

void setup() {
  // Setup dos pinos
  pinMode(LED_PIN, INPUT);
  pinMode(LED_LED, OUTPUT);
  pinMode(LED_RELAY, OUTPUT);
  pinMode(PUMP_PIN, INPUT);
  pinMode(PUMP_LED, OUTPUT);
  pinMode(PUMP_RELAY, OUTPUT);
  pinMode(BUZZER, OUTPUT);
 
  Serial.begin(9600);
  rtc.begin();

  // Toca buzina de inicio
  tone(BUZZER, 2250);
  delay(1000);
  noTone(BUZZER);
 
  //Desliga as tomadas do LED e da Bomba
  digitalWrite(LED_RELAY, HIGH);
  digitalWrite(PUMP_RELAY, HIGH);

  // Carrega os dados da bomba
  data.pstart = read(0);
  data.pduration = read(1);
  data.pfinish = read(2);

  //Setup do LED
  data.lstart = 0;
  data.lduration = 0;
  data.lfinish = read(3);

  Serial.println("INICIADO");
  Serial.println(rtc.getUnixTime(rtc.getTime()));
  Serial.println(rtc.getTimeStr());

}

int filter = LOW;
long now = 0;

void loop() {
  now = rtc.getUnixTime(rtc.getTime());

  fleitura = digitalRead(FILTER_PIN);
  lleitura = digitalRead(LED_PIN);
  pleitura = digitalRead(PUMP_PIN);

  if (pleitura == HIGH || lleitura == HIGH){
    tone(BUZZER, 3550);  
  } else {
    noTone(BUZZER);
  }


  if (lleitura == HIGH && lprevious == LOW) {
    if (canChangeLed() == 1) {
      if (lstatus == LOW) {
          lstatus = HIGH;
          if ((now - data.lfinish) > 120) // mantem a duração anterior se ligou com menos de 2 minutos
            data.lduration = 0;
            
          data.lstart = now;
          data.lfinish = 0;
          Serial.println("LED LIGADO");
          Serial.println(now);
      } else {
          lstatus = LOW;
          data.lstart = 0;
          data.lfinish = now;
          write(3, now);
          Serial.println("LED DESLIGADO");
          Serial.println(now);
      }
      
      lprevious = HIGH;
    } else {
      tone(BUZZER, 990);
      delay(500);
      noTone(BUZZER);
    }
  } else if (lleitura == LOW && lprevious == HIGH)
    lprevious = LOW;

  if (pleitura == HIGH && pprevious == LOW) {
    if (canChangePump() == 1){
      if (pstatus == LOW) 
          startPump();
      else 
          stopPump();
          
    } else {
      tone(BUZZER, 990);
      delay(500);
      noTone(BUZZER);
    }

    pprevious = HIGH;
  } else if (pleitura == LOW && pprevious == HIGH)
    pprevious = LOW;

  if (fleitura == HIGH && fprevious == LOW) {
    if (filter == LOW) {
      filter = HIGH;
      startPump();
    
    } else 
       stopPump();
    

     tone(BUZZER, 2500);
     delay(200);
     noTone(BUZZER);
     fprevious = HIGH;
  } else if (fleitura == LOW && fprevious == HIGH)
    fprevious = LOW;

  checkStartPump();
  checkStopPump();
  checkStopLed();
  
  digitalWrite(LED_LED, lstatus);
  digitalWrite(LED_RELAY, lstatus);
  digitalWrite(PUMP_LED, pstatus);
  digitalWrite(PUMP_RELAY, pstatus);
  digitalWrite(FILTER_LED, filter);
}

//LED Pode ligar ou desligar a qualquer momento
int canChangeLed(){
   return 1; //Pode desligar a qualquer momento
}

int canChangePump(){
  if (filter == HIGH) // Se a filtragem está ligada, não pode desligar pelo botão comum
    return 0;
    
  if (pstatus == HIGH) 
    return now - data.pstart > 60; //Só pode desligar depois de 2 minutos ligada
  
   return (data.pduration < 9000 || now - data.pfinish > 2400) && (now - data.pfinish) > 60;
    // Pode ligar se a duração for menor que 2h30m ou se já passou 40minutos desde que desligou da ultima vez e que, em ambas as situações, a bomba não tenha sido desligada nos ultimos 2 minutos
}


String rtcTime;

void checkStartPump(){  
  if (pstatus == HIGH) return;

  rtcTime = String(rtc.getTimeStr());
  
  if (rtcTime.equals("11:00:00") || rtcTime.equals("19:00:00")) // às 11 e as 19, chega se a piscina ficou ligada o suficiente
      if (now - data.pfinish > 75600 || data.pduration < 7200)
        startPump();
}

long plast = 0;
long patual = 0;

void checkStopPump(){
  if (pstatus == HIGH) { // Bomba ligada
    if ((filter == HIGH && data.pduration > 14400 ) || (filter == LOW && data.pduration > 9000) ) { // 4h filtrando ou 2h30 normal
      stopPump();
    } else {    
      patual = now - data.pstart;
      if (patual != plast) {
        data.pduration++;
        plast = patual;
        write(1, data.pduration);
      }
    }
  }
}

void startPump(){
  pstatus = HIGH;
          
  if (now - data.pfinish > 600) // Mantem a duração anterior se desligou com menos de 10 minutos
    data.pduration = 0;
    
  data.pstart = now;
  data.pfinish = 0;

  write(0, now);
  write(1, data.pduration);
  write(2, 0);

  if (filter == HIGH)
    Serial.println("FILTRAGEM LIGADA");
  
  Serial.println("BOMBA LIGADA");
  Serial.println(now);


}

void stopPump(){
  if (filter == HIGH)
    Serial.println("FILTRAGEM DESLIGADA");
  
  data.pstart = 0;
  data.pfinish = now;
  pstatus = LOW;
  filter = LOW;
  
  write(0, 0);
  write(2, now);
  Serial.println("BOMBA DESLIGADA");
  Serial.println(now);
}

long llast = 0;
long latual = 0;

//Valida se deve desligar o rele do LED
void checkStopLed(){
  if (lstatus == HIGH) 
    if (data.lduration > 28800) { //8 horas
      data.lstart = 0;
      data.lfinish = now;
      lstatus = LOW;
      write(4, now);

      Serial.println("LED DESLIGADO");
      Serial.println(now);
    } else {
      latual = now - data.lstart;
      if (latual != llast) {
        data.lduration++;
        llast = latual;
      }
    }
}


//Escreve na memória do Arduino
void write(int address, long value) {
  address *= 4;
  byte four = (value & 0xFF);
  byte three = ((value >> 8) & 0xFF);
  byte two = ((value >> 16) & 0xFF);
  byte one = ((value >> 24) & 0xFF);
  
  EEPROM.write(address, four);
  EEPROM.write(address + 1, three);
  EEPROM.write(address + 2, two);
  EEPROM.write(address + 3, one);
}

//Lê da memória do Arduino
long read(long address) {
  address *= 4;
  long four = EEPROM.read(address);
  long three = EEPROM.read(address + 1);
  long two = EEPROM.read(address + 2);
  long one = EEPROM.read(address + 3);
  
  return ((four << 0) & 0xFF) + ((three << 8) & 0xFFFF) + ((two << 16) & 0xFFFFFF) + ((one << 24) & 0xFFFFFFFF);
}
