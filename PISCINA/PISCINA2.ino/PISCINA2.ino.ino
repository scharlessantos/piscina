#include <EEPROM.h>

struct Data {
  long pump_start;
  long pump_aliveTime;
  long pump_finish;
  long pump_dayTime;
  long pump_dayStart;
  
 
  long led_start;
  long led_stop;
  long led_buttonPressed;
  
} data;

//DS3231 rtc(SDA, SCL);

//Pins to push buttons
const int LED_BUTTON_PIN = 2;
const int PUMP_BUTTON_PIN = 3;
const int FILTER_BUTTON_PIN = 4;

//Pins to status
const int LED_STATUS_PIN = 5;
const int PUMP_STATUS_PIN = 6;
const int FILTER_STATUS_PIN = 7;

//Pins to RELAY
const int LED_RELAY_PIN = 8;
const int PUMP_RELAY_PIN = 9;

// Notifying buzzer
const int BUZZER_PIN = 13;

// Led control
long led_time = 0;
byte led_previous = LOW;
byte led_status = LOW;
volatile byte led_button_status = LOW;
volatile byte led_button_long = LOW;

// Pump control
int pump_time = 0;
byte pump_previous = LOW;
byte pump_status = LOW;
byte filter_previous = LOW;
byte filter_status = LOW;
volatile byte pump_button_status = LOW;
volatile byte filter_button_status = LOW;

void setup() {
  //set pins
  pinMode(LED_BUTTON_PIN, INPUT);
  pinMode(PUMP_BUTTON_PIN, INPUT);
  pinMode(FILTER_BUTTON_PIN, INPUT);
  pinMode(LED_STATUS_PIN, OUTPUT);
  pinMode(PUMP_STATUS_PIN, OUTPUT);
  pinMode(FILTER_STATUS_PIN, INPUT);

  // Interruption for LED
  
  // Comunication with Serial Monitor
  Serial.begin(9600);
  Serial.println("Starting...");
  tone(BUZZER_PIN, 2250);
  delay(1000);
  noTone(BUZZER_PIN);

  // Load pump saved data
  data.pump_start = read(0);
  data.pump_aliveTime = read(1);
  data.pump_finish = read(2);

  
}

void loop() {

}



//Write to Arduino Memory
void write(int address, long value) {
  noInterrupts();
  address *= 4;
  byte four = (value & 0xFF);
  byte three = ((value >> 8) & 0xFF);
  byte two = ((value >> 16) & 0xFF);
  byte one = ((value >> 24) & 0xFF);
  
  EEPROM.write(address, four);
  EEPROM.write(address + 1, three);
  EEPROM.write(address + 2, two);
  EEPROM.write(address + 3, one);
  interrupts();
}

//Read from Arduino Memory
long read(long address) {
  noInterrupts();
  address *= 4;
  long four = EEPROM.read(address);
  long three = EEPROM.read(address + 1);
  long two = EEPROM.read(address + 2);
  long one = EEPROM.read(address + 3);
  interrupts();  
  return ((four << 0) & 0xFF) + ((three << 8) & 0xFFFF) + ((two << 16) & 0xFFFFFF) + ((one << 24) & 0xFFFFFFFF);
}
